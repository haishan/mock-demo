'use strict';

/* eslint-disable no-console */

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const PORT = parseInt(process.env.PORT || 3000);
// should follow the pattern: x-mock-{service_name}
const MOCK_HEADER_STR = 'x-mock-github';

//--- main

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const payloadGetUsersDefault = {
  login: 'octocat',
  id: 583231,
  avatar_url: 'https://avatars3.githubusercontent.com/u/583231?v=4',
  gravatar_id: '',
  url: 'https://api.github.com/users/octocat',
  html_url: 'https://github.com/octocat',
  followers_url: 'https://api.github.com/users/octocat/followers',
  following_url: 'https://api.github.com/users/octocat/following{/other_user}',
  gists_url: 'https://api.github.com/users/octocat/gists{/gist_id}',
  starred_url: 'https://api.github.com/users/octocat/starred{/owner}{/repo}',
  subscriptions_url: 'https://api.github.com/users/octocat/subscriptions',
  organizations_url: 'https://api.github.com/users/octocat/orgs',
  repos_url: 'https://api.github.com/users/octocat/repos',
  events_url: 'https://api.github.com/users/octocat/events{/privacy}',
  received_events_url: 'https://api.github.com/users/octocat/received_events',
  type: 'User',
  site_admin: false,
  name: 'The Octocat',
  company: 'GitHub',
  blog: 'http://www.github.com/blog',
  location: 'San Francisco',
  email: null,
  hireable: null,
  bio: null,
  public_repos: 7,
  public_gists: 8,
  followers: 2131,
  following: 5,
  created_at: '2011-01-25T18:44:36Z',
  updated_at: '2018-02-23T04:10:52Z'
};

const payloadGetUsers404 = {
  message: 'Not Found',
  documentation_url: 'https://developer.github.com/v3/users/#get-a-single-user'
};

app.get('/users/:username', (req, res) => {
  const mockHeader = req.get(MOCK_HEADER_STR);
  res.set('X-Example-Header', 'hello');
  switch (mockHeader) {
    case '404': {
      return res.status(404).json(payloadGetUsers404);
    }

    default: {
      return res.json(payloadGetUsersDefault);
    }
  }
});

app.listen(PORT, '0.0.0.0', err => {
  if (err) console.log(err);

  console.log(`> server listening on http://0.0.0.0:${PORT}`);
});
