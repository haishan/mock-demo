#!/bin/bash

docker-compose up -d > /dev/null 2>&1

if which jq > /dev/null 2>&1; then
  JQ_BIN=jq
else
  JQ_BIN=cat
fi

request() {
  cmd=${@}

  echo
  echo " =============== "
  echo " $ "${@}
  echo " =============== "
  echo

  "${@}" | ${JQ_BIN}
}


request curl -sS "localhost:8000/users/octocat" -H "host: github_mock"
request curl -sS "localhost:8000/users/octocat" -H "host: github_mock" -H "x-mock-github: 404"

request curl -sS "localhost:8000/v0/item/8863.json" -H "host: hacker_news_mock"
request curl -sS "localhost:8000/v0/item/8863.json" -H "host: hacker_news_mock" -H "x-mock-hacker_news: 500"

printf "\n\n-> stop and remove containers with: docker-compose down\n"
